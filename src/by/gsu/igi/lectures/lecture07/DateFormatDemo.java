package by.gsu.igi.lectures.lecture07;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Evgeniy Myslovets
 */
public class DateFormatDemo {

    public static void main(String[] args) throws ParseException {
        Date date = new Date();
        System.out.println(date);

        Format format = DateFormat.getInstance();
        System.out.println(format.format(date));

        format = DateFormat.getDateInstance();
        System.out.println(format.format(date));

        Locale.setDefault(new Locale("ru"));
        System.out.println(DateFormat.getDateInstance().format(date));
        System.out.println(DateFormat.getDateInstance(DateFormat.SHORT).format(date));
        System.out.println(DateFormat.getDateInstance(DateFormat.LONG).format(date));

        System.out.println(DateFormat.getTimeInstance().format(date));
        System.out.println(DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
        System.out.println(DateFormat.getTimeInstance(DateFormat.LONG).format(date));

        DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        System.out.println(df1.format(date));
        DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println(df2.format(date));

        date = df2.parse("24.03.2014");
        System.out.println(df1.format(date));
    }
}